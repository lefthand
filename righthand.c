#include <windows.h>

int main() {
    DWORD dwZero = 0;
	HKEY hkMouse;
	
	SwapMouseButton(FALSE);
	if (!RegCreateKeyEx(HKEY_CURRENT_USER, "Control Panel\\Mouse", 0, NULL,
						0, KEY_SET_VALUE, NULL, &hkMouse, NULL))
		RegSetValueEx(hkMouse, "SwapMouseButtons", 0, REG_DWORD, (LPBYTE)&dwZero, sizeof(DWORD));
}

#include <windows.h>

int main() {
	DWORD dwOne = 1;
	HKEY hkMouse;
	
	SwapMouseButton(TRUE);
	if (!RegCreateKeyEx(HKEY_CURRENT_USER, "Control Panel\\Mouse", 0, NULL,
						0, KEY_SET_VALUE, NULL, &hkMouse, NULL))
		RegSetValueEx(hkMouse, "SwapMouseButtons", 0, REG_DWORD, (LPBYTE)&dwOne, sizeof(DWORD));
}